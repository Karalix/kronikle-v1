import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import fr from 'vuetify/src/locale/fr';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#1a237e',
        secondary: '#ffe0b2',
        accent: '#ffe0b2',
      },
    },
  },
  lang: {
    locales: { fr },
    current: 'fr',
  },
});
