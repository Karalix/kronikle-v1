import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import store from '../store/index'
import NewEvent from '../views/NewEvent.vue'
import EditEvent from '../views/EditEvent.vue'
import MyEvents from '../views/MyEvents.vue'
import Displays from '../views/Displays.vue'
import EditDisplay from '../views/EditDisplay.vue'
import Register from '../views/Register.vue'
import Pleaseconfirm from '../views/Pleaseconfirm.vue'
import PlacesList from '../views/PlacesList.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/my-events',
    name: 'MyEvents',
    component: MyEvents
  },
  {
    path: '/places',
    name: 'Places',
    component: PlacesList
  },
  {
    path: '/display',
    name: 'Displays',
    component: Displays
  },
  {
    path: '/display/:id',
    name: 'DisplayEdit',
    component: EditDisplay
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/pleaseconfirm',
    name: 'Pleaseconfirm',
    component: Pleaseconfirm
  },
  {
    path: '/new-event',
    name: 'NewEvent',
    component: NewEvent
  },
  {
    path: '/edit-event/:id',
    name: 'EditEvent',
    component: EditEvent
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (!store.state.auth.connected && !(to.path.includes('/login') || to.path.includes('/register') || to.path.includes('/pleaseconfirm'))) {
    next({
      path: '/login'
    })
  } else {
    next()
  }
})

export default router
