import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import PouchDB from 'pouchdb-browser'
import PouchFind from 'pouchdb-find'
import PouchLiveFind from 'pouchdb-live-find'
import PouchVue from 'pouch-vue'
import i18n from './i18n'
import PouchAuthentication from 'pouchdb-authentication'
import vuetify from './plugins/vuetify'
import VCalendar from 'v-calendar'
import VueMasonry from 'vue-masonry-css'
import Vuelidate from 'vuelidate'

PouchDB.plugin(PouchFind)
PouchDB.plugin(PouchLiveFind)
PouchDB.plugin(PouchAuthentication)

Vue.config.productionTip = false

Vue.use(PouchVue, {
  pouch: PouchDB,    // optional if `PouchDB` is available on the global object,
  defaultDB: 'pouchdb', // optional, default is 'pouchdb'
})

Vue.use(VueMasonry)
Vue.use(Vuelidate)

Vue.use(VCalendar, {
  componentPrefix: 'vc',
})

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount('#app')
