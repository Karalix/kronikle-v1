enum KResourceType {
  url = 'url',
  file= 'file',
  ownDocument= 'owndocument',
  text= 'text',
  externalwidget= 'externalwidget'
}

interface KResource {
/*(String) _id : {eventid} + "-resource:" + Identifiant de la ressource
(String) type : "resource"
(String) resourcetype : ["url", "file", "owndocument", "text", "externalwidget"]

*Optional*
(String) url : URL de la ressource si type "url"
(MultiString) name : Nom de la ressource
(MultiString) description : Description de la ressource
(String) group : Nom du groupe auquel appartient la ressource (spécifique à l'événement)
(Boolean) ownResource : si la ressource provient de l'institution ou de l'extérieur
(String) imageurl : image d'illustration de la ressource
(String) author : {authorid} Identifiant de l'auteur de la ressource*/
  _id: string,
  type: string,
  resourcetype: KResourceType,
  url?: string,
  name?: string,
  description?: string,
  group?: string,
  ownResource?: boolean,
  imageurl?: string,
  author?: string
}

interface KDisplay {
/*(String) _id : "display:" + Identifiant du display
(String) type : "display"
(String) name : nom de l'affichage

*Optional*
displayurl : url d'accès à l'affichage du display
template : template d'affichage du display ["explore2", "interactive poster", "webcalendar", etc.]
events : Liste des événements attachés au display
eventfilter : requête de filtrage des événements à afficher
*/
  _id: string,
  type: string,
  name: string,
  displayurl?: string,
  template?: string,
  events: string[],
  eventfilter?: string
}

export type {
  KResource,
  KDisplay,
}