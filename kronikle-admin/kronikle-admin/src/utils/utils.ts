const getClosestUpcomingDate = function (dates: Array<Date>, now: Date) : Date | null {
  if (!dates || !now) {
    return null
  }
  
  const day = now
  day.setHours(0, 0, 0, 0)

  let closestUpcomingDate: Date | null = null

  const sortedOccurences = dates.sort((a, b) => {
    return a.getTime() - b.getTime()
  })

  for (const occurence of sortedOccurences) {
    if (occurence.getTime() < day.getTime()) {
      continue
    } else {
      closestUpcomingDate = occurence
    }
  }

  if (closestUpcomingDate === null) {
    return dates[dates.length - 1]
  } else {
    return closestUpcomingDate
  }
}

/**
 * Check if date is today
 */
const isDateToday = function (date: Date): boolean {
  return date.getDate() === new Date().getDate() && date.getMonth() === new Date().getMonth() && date.getFullYear() === new Date().getFullYear()
}

/**
 * Get string closest string available for locale
 */
const getClosestLocaleString = function (localeObject: any, preferredLocale: string, fallbackLocale: string): string {
  const keys = Object.keys(localeObject)
  // if localeObject has no keys, return an empty string
  if (keys.length === 0) {
    return ''
  }
  // if localeObject has only one key, return it
  if (keys.length === 1) {
    return localeObject[keys[0]]
  }
  // if localeObject has key corresponding to preferredLocale, return it
  if (keys.includes(preferredLocale)) {
    return localeObject[preferredLocale]
  }
  // if localeObejct has key corresponding to fallbackLocale, return it
  if (keys.includes(fallbackLocale)) {
    return localeObject[fallbackLocale]
  }
  // if none of the above, try to return the 'en' string
  if (keys.includes('en')) {
    return localeObject['en']
  }
  // if no 'en' key, return the first key available
  return localeObject[keys[0]]
}

/**
 * Get an unique base 62 id not present in the pouchdb database
 */
const getUniqueId = function (db: any, partition: string): Promise<string> {
  return new Promise((resolve, reject) => {
    const base62chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    let id = partition
    for (let i = 0 ; i<6 ; i++) id += base62chars[Math.floor(Math.random() * base62chars.length)]
    db.get(id).then((doc: any) => {
      if (doc) {
        return getUniqueId(db, partition)
      } else {
        return resolve(id)
      }
    }
    ).catch(() => {
      return resolve(id)
    })
  })
}

/**
 * Transform a search string to return accentuated characters
 */
const getAccentFuzzyRegex = function (regex: string): string {
  let accentRegex = ''
  for (let i = 0 ; i < regex.length ; i++) {
    switch (regex[i]) {
      case 'a':
        accentRegex += '[a@À-Åà-åĀ-ąǍǎǞ-ǡǺ-ǻȀ-ȃȦȧȺɐ-ɒḀḁẚẠ-ặ]'
        break
      case 'b':
        accentRegex += '[bßƀ-ƅɃɓḂ-ḇ]'
        break
      case 'c':
        accentRegex += '[cçÇĆ-čƆ-ƈȻȼɔḈḉ]'
        break
      case 'd':
        accentRegex += '[dÐðĎ-đƉ-ƍȡɖɗḊ-ḓ]'
        break
      case 'e':
        accentRegex += '[eÈ-Ëè-ëĒ-ěƎ-ƐǝȄ-ȇȨȩɆɇɘ-ɞḔ-ḝẸ-ệ]'
        break
      case 'f':
        accentRegex += '[fƑƒḞḟ]'
        break
      case 'g':
        accentRegex += '[gĜ-ģƓǤ-ǧǴǵḠḡ]'
        break
      case 'h':
        accentRegex += '[hĤ-ħƕǶȞȟḢ-ḫẖ]'
        break
      case 'i':
        accentRegex += '[iÌ-Ïì-ïĨ-ıƖƗǏǐȈ-ȋɨɪḬ-ḯỈ-ị]'
        break
      case 'j':
        accentRegex += '[jĴĵǰȷɈɉɟ]'
        break
      case 'k':
        accentRegex += '[kĶ-ĸƘƙǨǩḰ-ḵ]'
        break
      case 'l':
        accentRegex += '[l£Ĺ-łƚȴȽɫ-ɭḶ-ḽ]'
        break
      case 'm':
        accentRegex += '[mƜɯ-ɱḾ-ṃ]'
        break
      case 'n':
        accentRegex += '[nÑñŃ-ŋƝƞǸǹȠȵɲ-ɴṄ-ṋ]'
        break
      case 'o':
        accentRegex += '[oÒ-ÖØò-öøŌ-őƟ-ơǑǒǪ-ǭǾǿȌ-ȏȪ-ȱṌ-ṓỌ-ợ]'
        break
      case 'p':
        accentRegex += '[pƤƥṔ-ṗ]'
        break
      case 'q':
        accentRegex += '[qɊɋ]'
        break
      case 'r':
        accentRegex += '[rŔ-řƦȐ-ȓɌɍṘ-ṟ]'
        break
      case 's':
        accentRegex += '[s$Ś-šƧƨȘșȿṠ-ṩ]'
        break
      case 't':
        accentRegex += '[tŢ-ŧƫ-ƮȚțȾṪ-ṱẗ]'
        break
      case 'u':
        accentRegex += '[uÙ-Üù-üŨ-ųƯ-ƱǓ-ǜȔ-ȗɄṲ-ṻỤ-ự]'
        break
      case 'v':
        accentRegex += '[vƲɅṼ-ṿ]'
        break
      case 'w':
        accentRegex += '[wŴŵẀ-ẉẘ]'
        break
      case 'x':
        accentRegex += '[xẊ-ẍ]'
        break
      case 'y':
        accentRegex += '[yÝýÿŶ-ŸƔƳƴȲȳɎɏẎẏỲ-ỹỾỿẙ]'
        break
      case 'z':
        accentRegex += '[zŹ-žƵƶɀẐ-ẕ]'
        break
      default:
        accentRegex += regex[i]
    }
  }
  return accentRegex
}

export {getClosestUpcomingDate, isDateToday, getClosestLocaleString, getUniqueId, getAccentFuzzyRegex}