import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      username: '',
      password: ''
    },
    auth: {
      token: '',
      connected: false,
      sync: null as any,
    },
    db: {
      name: 'kronikle',
      userdb: 'kronikle',
      url: `${window.location.protocol}//${window.location.hostname}:5984/kronikle`,
    },
    kronikleserverurl: `${window.location.protocol}//${window.location.host}`,
    kronikleapiurl: `${window.location.protocol}//${window.location.hostname}:3000`,
  },
  mutations: {
    logout (state) {
      state.user.username = ''
      state.user.password = ''
      state.auth.connected = false
      state.auth.sync?.cancel()
    },
    setSync (state, sync) {
      state.auth.sync = sync
    },
  },
  actions: {
    async login (context, { username, password, vue }) {
      try {
        const logindata = await fetch(`${context.state.kronikleapiurl}/auth/login`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            username,
            password
          })
        }).then(res => res.json())

        if (logindata.error) {
          return
        }

        if (logindata.userDBs && logindata.userDBs.kronikle) {
          context.state.db.url = logindata.userDBs.kronikle
          context.state.db.userdb = 'kronikle$' + logindata.userDBs.kronikle.split('kronikle$')[1]
          context.state.db.name = 'pouchdb'
          context.state.user.username = logindata.user_id
          context.state.auth.connected = true


          const sync = vue.$pouch.sync('pouchdb', logindata.userDBs.kronikle, {
            live: true,
            retry: true,
            retryDelay: 1000,
            retryMax: 5
          })
          .on('active', () => {
            console.log('Connected !')
          })
          .on('denied', (info: any) => {
            context.commit('logout')
            vue.$router.push('/login')
            console.log('Access denied : ')
            console.log(info)
          })
          .on('error', (err: any) => {
            context.commit('logout')
            vue.$router.push('/login')
            console.log(err)
          })
          
          context.commit('setSync', sync)
          vue.$router.push('/')
        }
      } catch (e) {
        console.log(e)
      }

      
    },
    logout (context, vue) {
      context.commit('logout')
      vue.$router.push('/login')
    }
  },
  modules: {
  }
})
