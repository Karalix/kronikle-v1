var express = require('express')
var router = express.Router()
var getdb = require('../db/db')
const helpers = require('../helpers/helper.js')

/* GET the list of events */
router.get('/:userkey/event', async function(req, res, next) {
  /**
   * TODO: replace this basic list with a View to retrieve the name, the occurences, the tags, and the place
   */
  try {
    let db = await getdb(req.params.userkey)
    let result = await db.partitionedList('event')
    res.send(result.rows.map(e => {
      return {
        id: e.id,
      }
    }))
  } catch (e) {
    res.status(500).send(e.error)
  }
})

/* GET an event */
router.get('/:userkey/event/:eventid', async function(req, res, next) {
  if ((!req.params.eventid || !req.params.eventid.startsWith('event:')) || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let result = await db.get(req.params.eventid)
      res.send(result)
    } catch (e) {
      res.status(404).send(e.error)
    }
  }
})

/* GET a place */
router.get('/:userkey/place/:placeid', async function(req, res, next) {
  if ((!req.params.placeid || !req.params.placeid.startsWith('place:')) || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let result = await db.get(req.params.placeid)
      res.send(result)
    } catch (e) {
      res.status(404).send(e.error)
    }
  }
})


/* GET the list of displays */
router.get('/:userkey/display', async function(req, res, next) {
  try {
    let db = await getdb(req.params.userkey)
    let result = await db.partitionedList('display', {include_docs: true})
    res.send(result.rows.map(e => {
      return e.doc
    }))
  } catch (e) {
    res.status(500).send(e.error)
  }
})


/* GET a display */
router.get('/:userkey/display/:displayid', async function(req, res, next) {
  if ((!req.params.displayid || !req.params.displayid.startsWith('display:')) || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let result = await db.get(req.params.displayid)
      res.send(result)
    } catch (e) {
      res.status(500).send(e.error)
      console.log(e)
    }
  }
})


/* GET a display's attached events and events from filters */
router.get('/:userkey/display/:displayid/all-events', async function(req, res, next) {
  if (!req.params.displayid || !req.params.displayid.startsWith('display:') || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    let events = []
    try {
      let db = await getdb(req.params.userkey)
      let display = await db.get(req.params.displayid)
      if (display.events && display.events.length > 0) {
        let specificevents = await db.fetch({keys: display.events})
        events.push(...specificevents.rows.map(e => e.doc))
      }
      // eventtimefilter was previously eventfilter
      let timeFilter = display.eventtimefilter ?? display.eventfilter

      // Add events based on the event filter of the display
      if (timeFilter && timeFilter != 'none') {
        let timeresult = null


        let queryOptions = helpers.getTimeFilterOptionObject(timeFilter)
        if (queryOptions) {
          timeresult = await db.partitionedView('event', 'events', 'dates', queryOptions)
        }

        if (timeresult) {
          if (display.tagfilter) {
            /**
             * TODO: Manage tag filter
             */
          }
          timeresult.rows.map(eventobj => {
            let doc = eventobj.doc
            events.push(doc)
          })
        }
      }

      res.send(events)
    } catch (e) {
      console.error(e)
      res.status(500).send(e)
    }
  }
})

/* GET resources for an event */
router.get('/:userkey/event/:eventid/resources', async function(req, res, next) {
  if (!req.params.eventid || !req.params.eventid.startsWith('event:') || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let result = await db.get(req.params.eventid)
      let resourceids = []
      if (result.resourceOrder) {
        resourceids = result.resourceOrder.resources
      } else if (result.resources) {
        resourceids = result.resources
      }

      if (resourceids.length > 0) {
        let resources = await db.fetch({keys: resourceids})
        res.send(resources.rows.map(e => e.doc))
      } else {
        res.send([])
      }

    } catch (e) {
      res.status(500).send(e.error)
    }
  }
})

/* GET a resource */
router.get('/:userkey/resource/:resourceid', async function(req, res, next) {
  if (!req.params.resourceid || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let result = await db.get(req.params.resourceid)
      res.send(result)
    } catch (e) {
      res.status(500).send(e.error)
    }
  }
})


/* GET a resource file */
router.get('/:userkey/resource/:resourceid/file', async function(req, res, next) {
  if (!req.params.resourceid || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      db.attachment.getAsStream(req.params.resourceid, 'file')
        .on('error', err => {
          res.status(500).send(err.error)
        })
        .pipe(res)
    } catch (e) {
      console.log(e)
      res.status(500).send(e.error)
    }
  }
})


/* GET an event image*/
router.get('/:userkey/event/:eventid/image', async function(req, res, next) {
  if ((!req.params.eventid || !req.params.eventid.startsWith('event:')) || !req.params.userkey) {
    res.sendStatus(404)
  } else {
    try {
      let db = await getdb(req.params.userkey)
      let type = (await db.get(req.params.eventid))._attachments.image.content_type
      res.type(type)
      db.attachment.getAsStream(req.params.eventid, 'image')
        .on('error', e=> {
          console.log(e)
          res.status(404).send(e.error)
        })
        .pipe(res)
    } catch (e) {
      console.log(e)
      res.status(404).send(e.error)
    }
  }
})


module.exports = router