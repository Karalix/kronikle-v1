var express = require('express')
var router = express.Router()
const fetch = require('node-fetch')

/* GET the url */
router.get('/:url', function(req, res, next) {
  const decodedurl = decodeURIComponent(req.params.url)
  console.log(decodedurl)
  fetch(decodedurl)
    .then(response => {
      return response.text()
    })
    .then(text => {
      res.send(text)
    })
  //res.send('respond with a resource')
});

module.exports = router
