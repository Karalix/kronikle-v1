var express = require('express')
var router = express.Router()
const config = require('../config')

const OAImporter = require('../importer/OAImporter')

let opacadapter = null

/**
 * Check importorigin in the config file and one is set, call the adequate importer
 */
function importAgenda(userkey) {
  switch (config.import.importOrigin) {
    case 'openagenda':
        oaimporter = new OAImporter()
        oaimporter.import(userkey)
        break
    default:
        console.log('No import origin set')
        break
    }
}


/**
 * If import in enabled, call importAgenda every 10 minutes
 */
 if (config.import.importEnabled) {
  setInterval(importAgenda, 600000)
  importAgenda()
}

/**
 * Initialize the opac Adapter from the information present in the config file
 */
if (config.opac.opacEnabled) {
  switch (config.opac.opacOrigin) {
    case 'decalog':
        DecalogAdapter = require('../opac-adapters/DecalogAdapter');
        opacadapter = new DecalogAdapter(config.opac.opacBaseurl, config.opac.opacQueryUrlOne, config.opac.opacQueryUrlTwo)
        break
    default:
        console.log('No opac origin set')
        break
    }
}

/**
 * Express route to activate import routine if it is enabled to the 'kronikle' database
 */
/*
router.get('/import', (req, res) => {
  if (config.import.importEnabled) {
    importAgenda()
  }
  res.sendStatus(200)
})
*/


/**
 * Express route to activate import routine if it is enabled
 */
 router.get('/import/:userkey', (req, res) => {
  if (config.import.importEnabled && req.params.userkey) {
    importAgenda(req.params.userkey)
  }
  res.sendStatus(200)
})

/**
 * Express route to query the opac adapter
 */
router.get('/opac/:query', (req, res) => {
  if (config.opac.opacEnabled) {
    opacadapter.search(req.params.query)
    .then(result => {
      res.send(result)
    })
    .catch(err => {
      res.status(500).send(err)
    })
  } else {
    res.sendStatus(404)
  }
})

module.exports = router;
