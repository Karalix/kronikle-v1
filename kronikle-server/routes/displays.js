var express = require('express')
let getdb = require('../db/db.js')
var router = express.Router()
var fs = require('fs')
const path = require('path')
const helpers = require('../helpers/helper.js')
const config = require('../config.js')

const getClosestUpcomingDate = function (dates, now) {
  if (!dates || !now) {
    return null
  }
  
  const day = now
  day.setHours(0, 0, 0, 0)

  let closestUpcomingDate = null

  const sortedOccurences = dates.sort((a, b) => {
    return a.getTime() - b.getTime()
  })

  for (const occurence of sortedOccurences) {
    if (occurence.getTime() < day.getTime()) {
      continue
    } else {
      closestUpcomingDate = occurence
    }
  }

  if (closestUpcomingDate === null) {
    return dates[dates.length - 1]
  } else {
    return closestUpcomingDate
  }
}

const prepareEventDetail = async function (userkey, displayid, eventobj) {
  let preparedevent = await prepareEvent(userkey, displayid, eventobj)
  if (preparedevent.resourceOrder && preparedevent.resourceOrder.resources) {
    let db = await getdb(userkey)
    let resources = await db.fetch({keys: preparedevent.resourceOrder.resources})
    for (let resource of resources.rows) {
      resource.doc.publicurl = `${config.host}:${config.port}/api/${userkey}/resource/${resource.id}/file`
    }
    preparedevent.render.resources = resources.rows.map(re => re.doc)
    //console.log(preparedevent.render.resources)
  }
  return preparedevent
}

const prepareEvent = async function (userkey, displayid, eventobj, key) {
  let closestdate = null
  if (!key) {
    closestdate = getClosestUpcomingDate(eventobj.occurences.map(occ => {
      return new Date(occ.startdate)
    }), new Date())
  } else {
    closestdate = new Date(key)
  }

  for (let occ of eventobj.occurences) {
    if (occ.placeid) {
      try {
        let db = await getdb(userkey)
        db.get(occ.placeid).then(place => {
          occ.place = place
        })
      } catch (e) {
        console.error(e)
      }
    }
  }

  let url = ''
  if (displayid) {
    url = `/d/${userkey}/dis/${displayid}/e/${eventobj._id}`
  } else {
    url = `/d/${userkey}/qr/${eventobj._id}`
  }

  eventobj.render = {
    nboccurences: eventobj.occurences.length,
    url: url,
    closestdate: closestdate.toISOString(),
  }
  return eventobj
}

router.get('/:userkey/qr/:eventid', async function(req, res, next) {
  let eventid = req.params.eventid

    try {
      let db = await getdb(req.params.userkey)
      let displayOptions = {
        title: '',
        events: [],
        image: 'null', 
        imagedescription: 'null',
        url: `${req.protocol}://${req.hostname}${req.originalUrl}`,
      }

      let eventobj = await prepareEventDetail(req.params.userkey, null, await db.get(eventid))
      
      res.render('evdetail', {...displayOptions, event: eventobj, style: fs.readFileSync('./static-displays/styles/evlist.css', 'utf-8')})
            
      } catch (err) {
      res.statusCode = 404
      console.log(err)
      res.send('Display not found')
    }
})

router.get('/:userkey/dis/:displayid', async function(req, res, next) {
  let displayid = req.params.displayid

  let events = []
  try {
  let db = await getdb(req.params.userkey)
  let userkey = req.params.userkey

    db.get(displayid)
      .then(async display => {
        if ((display.events && display.events.length > 0) || (display.eventfilter && display.eventfilter != 'none')) {
          
          let displayOptions = {
            title: display.name,
            events: events,
            image: 'null', 
            imagedescription: 'null',
            url: `${req.protocol}://${req.hostname}${req.originalUrl}`,
          }

          for (const eventid of display.events) {
            events.push(await prepareEvent(userkey, displayid, await db.get(eventid)))
          }

          // eventtimefilter was previously eventfilter
          let timeFilter = display.eventtimefilter ?? display.eventfilter

          // Add events based on the event filter of the display
          if (timeFilter && timeFilter != 'none') {
            let result = null


            let queryOptions = helpers.getTimeFilterOptionObject(timeFilter)
            if (queryOptions) {
              result = await db.partitionedView('event', 'events', 'dates', queryOptions)
            }

            if (result) {
              result.rows.map(async eventobj => {
                let doc = eventobj.doc
                events.push(await prepareEvent(userkey, displayid, doc, eventobj.key))
              })
            }
          }

          events.sort((a,b) => {
            return a.render.closestdate - b.render.closestdate
          })

          switch (display.template) {
            case 'simplehtml':
              res.render('evlist', {...displayOptions, style: fs.readFileSync('./static-displays/styles/evlist.css', 'utf-8')})
              break
            case 'explorer':
              res.sendFile(path.join(__dirname, '../dynamic-displays/explorer/dist/index.html'))
              break
            default:
              res.render('evlist', {...displayOptions, style: fs.readFileSync('./static-displays/styles/evlist.css', 'utf-8')})
              break
          }
        }
      })
      .catch(err => {
        res.statusCode = 404
        console.log(err)
        res.send('Display not found')
      })
  } catch (err) {
    res.statusCode = 404
    console.log(err)
    res.send('Display not found')
  }
})

router.get('/:userkey/dis/:displayid/e/:eventid', async function(req, res, next) {
  let displayid = req.params.displayid
  let eventid = req.params.eventid
  let userkey = req.params.userkey
  let events = []

  let db = await getdb(userkey)

  db.get(displayid)
    .then(async display => {
      if (display.events && display.events.length > 0 || display.eventfilter && display.eventfilter != 'none') {
        
        let displayOptions = {
          title: display.name,
          events: events,
          image: 'null', 
          imagedescription: 'null',
          url: `${req.protocol}://${req.hostname}${req.originalUrl}`,
        }

        let eventobj = await prepareEventDetail(userkey, displayid, await db.get(eventid))
        
        switch (display.template) {
          case 'simplehtml':
            res.render('evdetail', {...displayOptions, event: eventobj, style: fs.readFileSync('./static-displays/styles/evlist.css', 'utf-8')})
            break;
        
          default:
            res.render('evdetail', {...displayOptions, event: eventobj, style: fs.readFileSync('./static-displays/styles/evlist.css', 'utf-8')})
            break;
        }
      }
    })
    .catch(err => {
      res.statusCode = 404
      console.log(err)
      res.send('Display not found')
    })
})

module.exports = router;