// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
 module.exports = {
  publicPath: '/d-explorer/',
  lintOnSave: false
 }
