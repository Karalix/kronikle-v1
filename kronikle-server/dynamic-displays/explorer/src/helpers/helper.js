const getClosestUpcomingDate = function (dates, now) {
  if (!dates || !now) {
    return null
  }
  
  const day = now
  day.setHours(0, 0, 0, 0)

  let closestUpcomingDate = null

  const sortedOccurences = dates.sort((a, b) => {
    return a.getTime() - b.getTime()
  })

  for (const occurence of sortedOccurences) {
    if (occurence.getTime() < day.getTime()) {
      continue
    } else {
      closestUpcomingDate = occurence
    }
  }

  if (closestUpcomingDate === null) {
    return dates[dates.length - 1]
  } else {
    return closestUpcomingDate
  }
}

const getTimeFilterOptionObject = function (timeFilter) {
  let result = null
  let todayBegin = new Date()
  todayBegin.setHours(0)
  todayBegin.setMinutes(0)
  todayBegin.setSeconds(0)
  todayBegin.setMilliseconds(0)

  switch (timeFilter) {
    case 'today':
      let tomorrowBegin = new Date(todayBegin)
      tomorrowBegin.setDate(tomorrowBegin.getDate()+1)
      tomorrowBegin.setMilliseconds(-1)

      result = {
        include_docs: true,
        startkey: todayBegin.toISOString(),
        endkey: tomorrowBegin.toISOString()
      }
      break;
    case 'thisweek':
      let beginWeek = new Date(todayBegin)
      if (beginWeek.getDay() === 0) {
        //if it is sunday, the beginning of the week is 6 days ago
        beginWeek.setDate(beginWeek.getDate()-6)
      } else {
        beginWeek.setDate(beginWeek.getDate()-(beginWeek.getDay()-1))
      }
      let endWeek = new Date(beginWeek)
      endWeek.setDate(endWeek.getDate()+6)

      result = {
        include_docs: true,
        startkey: beginWeek.toISOString(),
        endkey: endWeek.toISOString()
      }
      break;
    case 'thismonth':
      let beginMonth = new Date(todayBegin)
      beginMonth.setDate(1)
      let endMonth = new Date(beginMonth)
      endMonth.setMonth(endMonth.getMonth()+1)
      endMonth.setDate(1)

      result = {
        include_docs: true,
        startkey: beginMonth.toISOString(),
        endkey: endMonth.toISOString()
      }
      break;
    case 'upcoming':  
      result = {
        include_docs: true,
        startkey: todayBegin.toISOString(),
      }
      break;
    case 'past':  
      result = {
        include_docs: true,
        endkey: (new Date()).toISOString(),
      }
      break;
    case 'all':  
      result = {
        include_docs: true,
      }
      break;
    default:
      break;
  }
  return result
}

module.exports = {
  getClosestUpcomingDate,
  getTimeFilterOptionObject
}