module.exports = {
  "events": {
    "views": {
      "dates": {
        "map": "function (doc) {\n  if (doc.occurences) {\n    for (var occ of doc.occurences) {\n      emit(occ.startdate, {id: doc._id, name: doc.name, imageurl: doc.imageurl, status: doc.status, end: occ.enddate})\n    }\n  }\n}"
      },
      "tags": {
        "map": "function (doc) {\n  if (doc.tags && doc.tags.length > 0) {\n    for (var tag of doc.tags) {\n      emit(tag, {id: doc._id, name: doc.name, imageurl: doc.imageurl, status: doc.status})\n    }\n  }\n}"
      },
      "tag-list": {
        "reduce": "_count",
        "map": "function (doc) {\n  if (doc.tags && doc.tags.length > 0) {\n    for (const tag of doc.tags) {\n      emit(tag, 1)\n    }\n  }\n}"
      }
    },
    "language": "javascript",
    "options": {
      "partitioned": true
    }
  }
}