module.exports = {
  "_all_docs_resources_order": {
    "views": {
      "_all_docs_resources_order": {
        "map": "function (doc) {\n  emit(doc._id, {rev: doc._rev, resourceOrder: doc.resourceOrder});\n}"
      }
    },
    "language": "javascript",
    "options": {
      "partitioned": true
    }
  }
}