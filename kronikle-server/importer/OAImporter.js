let fetch = require('node-fetch')
let getdb = require('../db/db.js')
let config = require('./OAImporterconfig.js')
const logger = require('../logging.js')
var fs = require('fs')


class OAImporter {
  
  createEvent (event, rev, resourceOrder) {
    let eventObj = {
      _id: `event:${event.uid}`,
      name: event.title,
      desc: event?.longDescription ? event?.longDescription : {fr:'La description n\'a pas pu être importée'},
      shortdesc: event?.description ? event?.description : {fr: 'La description courte n\'a pas pu être importée'},
      datecreation: event.createdAt ? event.createdAt : (new Date).toISOString(),
      dateupdate: event.updatedAt ? event.updatedAt : (new Date).toISOString(),
      status: 'imported',
      type: 'event',
      author: 'imported',
      originStore: 'openagenda',
      originId: event.uid,
      imageurl: event.image && event.image.filename ? event.image.base + event.image.filename : null,
      occurences: [
        {
          startdate: event?.lastTiming?.begin,
          enddate: event?.lastTiming?.end,
          placeid: 'embedded',
          place: {
            name: event?.location?.name,
            latitude: event?.location?.latitude,
            longitude: event?.location?.longitude,
            address: event?.location?.address
          }
        }
      ]
    }

    if (rev !== undefined) {
      eventObj._rev = rev
    }

    if (resourceOrder !== undefined) {
      eventObj.resourceOrder = resourceOrder
    }

    return eventObj
  }

  import (userkey) {
    let nbevents = 0
    let initialfetchurl = `https://api.openagenda.com/v2/agendas/${config.agendaid}/events?key=${config.oakey}&size=`
    let oaevents = null
    let errorstep = 'initial fetch'
    let db = getdb(userkey)
    

    fetch(initialfetchurl + nbevents)
    .then(res => res.json())
    .then(body => {
      let totalEventsToImport = body.total
      errorstep = 'second fetch'
      if (totalEventsToImport > 0) {
        nbevents = totalEventsToImport
        return fetch(initialfetchurl + nbevents)
      } else {
        throw new Error('Could not parse the total number of events to retrieve from OpenAgenda API endpoint')
      }
    })
    .then(res => res.json())
    .then(body => {
      errorstep = 'db request'
      oaevents = body.events
      

      return db.partitionedView('event', '_all_docs_resources_order', '_all_docs_resources_order', {})
    })
    .then(response => {
      errorstep = 'db insert'
      let i = 0
      let existingEventMap = new Map()
      let bulkDocs = []

      for (i = 0 ; i < response.rows.length ; i++) {
        existingEventMap.set(response.rows[i].key, response.rows[i].value)
      }

      for (const event of oaevents) {
        let value = existingEventMap.get(`event:${event.uid}`)
        if(value !== undefined) {
          bulkDocs.push(this.createEvent(event, value.rev, value.resourceOrder))
        } else {
          bulkDocs.push(this.createEvent(event))
        }
      }
      return db.bulk({docs: bulkDocs})
    })
    .then(response => {
      let success = 0
      let fails = []

      for (const row of response) {
        if (row.ok) {
          success++
        } else {
          fails.push(row.id)
        }
      }

      if (response.length !== success) {
        logger.error({
          message: `Import failed for ${response.length-success} rows out of ${response.length} : ${fails}. ${JSON.stringify(response)}`
        })
      } else {
        logger.info({
          message: `Import succesful for all ${response.length} rows`
        })
      }
    })
    .catch(err => {
      logger.error({
        message: `Failed to import from OpenAgenda API Endpoint ${initialfetchurl}${nbevents} : ${err} at step ${errorstep}`
      })
    })
  }
}

module.exports = OAImporter
