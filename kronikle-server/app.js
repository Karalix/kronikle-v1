var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const config = require('./config')
var cors = require('cors')
var demodb = re = require('./db/db')

var bodyParser = require('body-parser')
var { SuperLogin } = require('@sl-nx/superlogin-next')

var { Liquid } = require('liquidjs')
var engine = new Liquid()


var corsRouter = require('./routes/cors')
var servicesRouter = require('./routes/services')
var displaysRouter = require('./routes/displays')
var api = require('./routes/api')

var app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())

/**
 * Superlogin-Next configuration
 */
var slconfig = {
  dbServer: {
    protocol: 'http://',
    host: 'localhost:5984',
    publicURL: config.dev ? 'http://localhost:5984' :  'http://vps-e57fc30a.vps.ovh.net:5984',
    user: 'admin',
    password: 'Vivelebrocoli',
    userDB: 'sl-users',
    couchAuthDB: '_users'
  },
  mailer: {
    fromEmail: 'authbot@kronikle.eu',
    options: {
      host: "admin.kronikle.eu",
      port: 587,
      secure: false,
      auth: {
        user: 'authbot',
        pass: 'DNfQb2XbdPF42q5'
      }
    }
  },
  userDBs: {
    defaultDBs: {
      private: ['kronikle']
    },
    designDocDir: path.join(__dirname, 'designdocs'),
    model: {
      _default: {
        designDocs: [
          'events',
          // TODO: verify the validation of the documents
          // 'validation',
          '_all_docs_resources_order'
        ],
        partitioned: true,
      }
    },
    userModel: {
      whitelist: ['institution']
    }
  }
}

// Initialize SuperLogin
var superlogin = new SuperLogin(slconfig)
app.use('/auth', superlogin.router)

app.engine('liquid', engine.express())
app.set('views', './static-displays')            // specify the views directory
app.set('view engine', 'liquid')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

// Serve static files for the Admin App (maybe temporary)
app.use(express.static(path.join(__dirname, '../kronikle-admin/kronikle-admin/dist')))
//serve static files for frontend Explorer
app.use('/d-explorer', express.static(path.join(__dirname, './dynamic-displays/explorer/dist')))


app.use('/services', servicesRouter)
app.use('/cors', corsRouter)
app.use('/d', displaysRouter)
app.use('/api', api)

app.listen(config.port, () => {
  console.log('Server listening on port ' + config.port)
})

module.exports = app

