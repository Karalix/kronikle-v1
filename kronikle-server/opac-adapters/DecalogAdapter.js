const fetch = require('node-fetch')
const jsdom = require('jsdom')

let DecalogAdapter = class DecalogAdapter {
  constructor(baseurl, queryurlone, queryurltwo) {
    this.baseurl = baseurl
    this.queryurlone = queryurlone
    this.queryurltwo = queryurltwo 
  }
  /**
   * Search de Opac with a string query
   */
  search(query) {
    let url = `${this.queryurlone}${query}${this.queryurltwo}`

    return fetch(url)
      .then(response => response.text())
      .then(text => {
        let documents = []
        const results = new jsdom.JSDOM(text)
        for(const result of results.window.document.querySelectorAll('.sid-result')) {
          //console.log(result.querySelector('.sid-infos-auteur'))
          let document = {
            title: result.querySelector('.sid-infos-title').textContent.trim(),
            author: result.querySelector('.sid-infos-auteur')?.textContent.trim() ?? result.querySelector('.sid-infos-editeur')?.textContent.trim(),
            cover: result.querySelector('.sid-image a img')?.getAttribute('src'),
            url: this.baseurl + result.querySelector('.sid-infos-title a').getAttribute('href')
          }

          documents.push(document)
        }
        return new Promise((resolve, reject) => {
          resolve(documents)
        })
      })
      .catch(err => {
        console.log(err)
      })
  }
}

module.exports = DecalogAdapter