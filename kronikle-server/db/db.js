let config = require('../config.js')
const couch = require('nano')(`http://${config.couchuser}:${config.couchpwd}@localhost:5984`)

//let events = couch.db.use('kronikle')

function getDB(userkey) {
  return couch.use('sl-users').find({
    selector: {
      type: 'user',
      key: userkey
    }
  }).then(res => {
    res = res.docs[0]
    return couch.use(`kronikle$${res._id}`)
  }).catch(err => {
    console.error('Base de donnée non trouvée, renvoi de la base legacy')
    console.error(err)
    return couch.use('kronikle')
  })
}

module.exports = getDB